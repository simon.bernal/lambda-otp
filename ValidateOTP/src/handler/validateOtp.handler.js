const { validateOtp } = require("./../app/validateOtp");
const { Response } = require("./../config/response");

const validateOtpHandler = async (event, context) => {
  try {
    const data = await validateOtp(event);
    return Response.success(data);
  } catch (error) {
    console.log("🚀 ~ saveHandler ~ error:", error);
    return Response.error(500, error);
  }
};

module.exports = { validateOtpHandler };