const { requestSaveShema } = require("./../config/schema/save.schema");
const { getItem } = require("./../service/dynamodb.service");
const { decryptItem } = require("./../config/encryption");
const tableName = process.env.SAMPLE_TABLE;

const validateOtp = async (body) => {
  try {
    const { error, value } = requestSaveShema.validate(body);
    console.log("🚀 ~ saveApp ~ error, value:", error, value)
    if (error) throw error ;
    const key = {
      transaction_id: value.transaction_id,
      document_number: value.document_number
    };
    const data = await getItem(tableName, key);
    const resultDecrypItem = await decryptItem(data,  ["transaction_id","document_number"]);
    let number_retries = Number(resultDecrypItem.number_retries);
    const expiration_date = Number(resultDecrypItem.expiration_date);
    const codOtp = value.codOtp;
    const otp = resultDecrypItem.otp;
    const timeNow = Date.now();
    if(codOtp === otp && number_retries <= 3 && (timeNow <= expiration_date) == true){
      number_retries++;
      return {
        validOtp: true
      };
    }else{
      return {
        validOtp: false
      };
    }
  } catch (error) {
    console.log("🚀 ~ getItem ~ error:", error)
    throw error;
  }
};
module.exports = { validateOtp };
