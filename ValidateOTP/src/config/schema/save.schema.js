const Joi = require("joi");

const requestSaveShema = Joi.object({
  document_number: Joi.string()
      .required(),
  transaction_id: Joi.string()
      .required(),
  codOtp: Joi.string()
      .required()
})

const response = {};

module.exports = {
  requestSaveShema
}