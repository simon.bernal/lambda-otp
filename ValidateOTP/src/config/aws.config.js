const { DynamoDBClient } = require("@aws-sdk/client-dynamodb");
const { DynamoDBDocumentClient, PutCommand, GetCommand } = require("@aws-sdk/lib-dynamodb");
const { KMSClient } = require("@aws-sdk/client-kms");

const client = new DynamoDBClient({});
const ddbDocClient = DynamoDBDocumentClient.from(client);
const KMS = new KMSClient();

module.exports = {
  ddbDocClient, PutCommand, GetCommand, KMS
}

