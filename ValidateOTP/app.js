const { validateOtpHandler } = require("./src/handler/validateOtp.handler");

const lambdaHandler = async (event, context) => {
  return await validateOtpHandler(event, context);
};

module.exports = { lambdaHandler };
