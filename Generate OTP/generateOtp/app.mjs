
import {handler} from "./src/handler/generateOtpHandler.js"
export const lambdaHandler = async (event, context) => {
  console.log("EVENT " + JSON.stringify(event));
  return await handler(event, context);
};
  