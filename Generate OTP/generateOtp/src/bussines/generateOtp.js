import { dynamoDBSaveItem } from "../services/dynamodb.service.js";
import { encryptItem } from "../config/utils/encryption.js"
import { toShema } from "../config/utils/schemaToObject.js";
export const generateOtp = async(event, context) => {
    try{
        const longitud_otp = event.dataResponse.Payload.data.dataResponse.longitud_otp;
        const duracion_otp = event.dataResponse.Payload.data.dataResponse.duracion_otp;
        const numeros = '0123456789';
        let otp = '';
        for (let i = 0; i < longitud_otp; i++) {
            const index = Math.floor(Math.random() * numeros.length);
            otp += numeros[index];
        }
        await saveOtp(event, otp, duracion_otp);
        return {
            codOtp: otp,
            otp:true
        };
    }catch(error){
        console.log(error);
        throw error;
    }
}

const saveOtp = async(event, otp, duracion_otp) =>{
    try{
        const TableName = process.env.TableName;
        const creationOtp = Date.now();
        const expirationOtp = creationOtp + (duracion_otp * 1000);
        const number_retries = "0";
        const data = {
            otp:otp.toString(),
            transaction_id: event.transaction_id,
            document_number: event.document_number,
            registration_date: creationOtp.toString(),
            expiration_date: expirationOtp.toString(),
            status_otp: "true",
            number_retries: number_retries
        };
        const item = toShema(data);
        const encryptedItem = await encryptItem(item, ["transaction_id","document_number"]);
        await dynamoDBSaveItem({ Item:encryptedItem, TableName:TableName });
    }catch(error){
        console.log(error);
        throw error;
    }
}
