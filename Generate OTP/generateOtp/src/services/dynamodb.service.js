import { ddb } from "../config/aws.config.js";
import { toObject, toShema } from "../config/utils/schemaToObject.js";

export const dynamoDBSaveItem = async ({ Item, TableName }) => {
  const params = { TableName, Item };
  return await ddb.putItem(params).promise();
};

export const dynamoDBGetItem = async ({ Search, TableName }) => {
  const Key = toShema(Search);
  const params = { TableName, Key };
  const response = await ddb.getItem(params).promise();
  return toObject(response.Item)
};
