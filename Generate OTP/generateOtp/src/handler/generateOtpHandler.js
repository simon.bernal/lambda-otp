import { Response } from "../config/utils/response.js";
import { generateOtp } from "../bussines/generateOtp.js"
export const handler = async (event, context) => {
    try{
        const data = await generateOtp(event);
        return Response.success(data);
    }catch(error){
        console.error(error);
        return Response.error(500, error);
    }
}