
const TYPE = {
    number:"N",
    string:"S",
    array:"SS"
  }
export const toShema = (object={}) => {
    const Item = Object.entries(object).map( ([key, value]) =>{
      const valueObject ={}
      valueObject[TYPE[typeof value]] = value
      return [key, valueObject]
    })
    return Object.fromEntries(Item)
  }
  
export const toObject= (shema={}) => {
    const Item = Object.entries(shema).map( ([key, value]) =>{
      const valueObject =Object.values(value)
      return [key, valueObject.shift()]
    })
    return Object.fromEntries(Item)
  }