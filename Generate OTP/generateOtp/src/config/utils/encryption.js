
import {KMS} from "../aws.config.js"

const ARN_KMS = "arn:aws:kms:us-east-1:877438593722:key/d20456bb-e709-4aa3-a67b-f965882aa3c0";

const TYPE = {
    number:"N",
    string:"S",
    array:"SS"
  }

  
/**
 * @function
 * @description Permite cifrar con kms un item para almacenar en una tabla en DynamoDB
 * @param {object} item Item de la tabla a la cual vamos a cifrar
 * @return {Promise} Promesa del servicio
 **/
export async function encryptItem(item, ignore) {
    const encryptedItem = {};
    //Preguntar como destructuro el value que esta en la key item
    for (const key in item) {
      if (ignore.includes(key)) {
        encryptedItem[key] = item[key];
      } else {
        const encryptedData = await KMS.encrypt({ KeyId: ARN_KMS, Plaintext: item[key].S }).promise();
        const encryptedValue = encryptedData.CiphertextBlob.toString('base64');
        encryptedItem[key] = { S: encryptedValue };
      }
    }
    return encryptedItem;
  }
  
  /**
   * @function
   * @description Permite descifrar con kms un item almacenado en una tabla en DynamoDB
   * @param {object} data Item de la tabla a la cual vamos a descifrar
   * @return {Promise} Promesa del servicio
   **/
  export async function decryptItem(data, ignore) {
    const decryptedItem = {};
    for (const key in data) {
      if (ignore.includes(key)) {
        decryptedItem[key] = data[key].S;
      } else {
        const ciphertextBlob = Buffer.from(data[key].S, 'base64');
        const decryptedData = await kms.decrypt({ KeyId: ARN_KMS, CiphertextBlob: ciphertextBlob }).promise();
        decryptedItem[key] = decryptedData.Plaintext.toString('utf-8');
      }
    }
    return decryptedItem;
  }