import AWS from "aws-sdk";
export { AWS };
export const ddb = new AWS.DynamoDB();
export const KMS = new AWS.KMS();
