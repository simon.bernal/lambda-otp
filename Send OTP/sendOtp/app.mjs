
import {sendOtpHandler} from "./src/handler/sendOtpHandler.js"

export const lambdaHandler = async (event, context) => {
  return await sendOtpHandler(event, context);
};
  