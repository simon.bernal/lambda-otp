import { sendEmail } from "../services/ses_sendemail.js";

export const sendOtp = async(event, context) =>{
    try{
        let emailDestination = [event.email];
        let subject = "codigo de verificacion";
        let text = "Su codigo de verificacion es " + event.dataResponse.Payload.data.codOtp;
        const data = await sendEmail(emailDestination, subject, text);
        console.log("Data sendEmail " + JSON.stringify(data));
        return {
            send: true
        };
    }catch(error){
        console.error(error);
        throw error;
    }
};