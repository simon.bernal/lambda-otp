export class Response {
    constructor(statusCode, message, data) {
      this.statusCode = statusCode;
      this.message = message;
      this.data = data;
    }
  
    static success(data, message = "Success") {
      return new Response(200, message, data);
    }
  
    static error(statusCode, message, data = null) {
      return new Response(statusCode, message, data);
    }
  }
