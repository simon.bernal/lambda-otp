// Load the AWS SDK for Node.js
import { SES } from "../config/aws.config.js";

export const sendEmail = async (emailDestination, subject, data) => {
    const params = {
        Destination: {
            ToAddresses: emailDestination,
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: `<p>${data}</p>`,
                }
            },
            Subject: {
                Charset: "UTF-8",
                Data: subject,
            },
        },
        Source: "johnnyaquintero@gmail.com",
    };
    return await SES.sendEmail(params).promise();
};
