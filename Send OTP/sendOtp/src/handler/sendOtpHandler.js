import {sendOtp} from "../bussines/sendOtp.js";
import { Response } from "../config/utils/response.js";
export const sendOtpHandler = async(event, context) =>{
    try{
        const data = await sendOtp(event, context);
        return Response.success(data);
    }catch(error){
        console.error(error);
        return Response.error(500, error);
    }
};